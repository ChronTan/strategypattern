package domain;

public class AppStrategy {
    public static void main(String[] args) {
        StrategyClient strategyClient = new StrategyClient();
        int[] massiv={1,3,2,9};
        int[] massiv2={10,3,2,9};
        strategyClient.setStrategy(new SortBubble());
        strategyClient.executeStrategy(massiv);
        strategyClient.setStrategy(new SortSelect());
        strategyClient.executeStrategy(massiv2);
    }
}
