package domain;

import java.util.Arrays;

public class SortBubble implements Sortingg {
    @Override
    public void sort(int[] arr) {
        System.out.println("Sort bubble ");
        System.out.println("do: "+ Arrays.toString(arr));
        boolean isSorted = false;
        int buf;
        while(!isSorted) {
            isSorted = true;
            for (int i = 0; i < arr.length-1; i++) {
                if(arr[i] > arr[i+1]){
                    isSorted = false;

                    buf = arr[i];
                    arr[i] = arr[i+1];
                    arr[i+1] = buf;
                }
            }
        }
        System.out.println("after: "+Arrays.toString(arr));

    }
}
