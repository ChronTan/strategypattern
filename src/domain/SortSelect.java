package domain;

import java.util.Arrays;

public class SortSelect implements Sortingg {
    @Override
    public void sort(int[] arr) {
        System.out.println("Sort select ");
        System.out.println("do: "+ Arrays.toString(arr));
        for (int k = 1; k < arr.length; k++) {
            int newElement = arr[k];
            int location = k - 1;
            while (location >= 0 && arr[location] > newElement) {
                arr[location + 1] = arr[location];
                location--;
            }
            arr[location + 1] = newElement;
        }
        System.out.println("after: "+Arrays.toString(arr));
    }
}
