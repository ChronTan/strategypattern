package domain;

//Context
public class StrategyClient {
    Sortingg strategy;
    public void setStrategy(Sortingg strategy){
        this.strategy=strategy;
    }
    public void executeStrategy(int[] arr ){
        strategy.sort(arr);
    }

}
